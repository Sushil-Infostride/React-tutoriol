import React from 'react'

export default function AlertSuccess(props) {
    return (
            <div class="alert alert-danger" role="alert">
                <h1>{props.success}</h1>
            </div>      
    )
}
