// import ReactDOM from 'react-dom';
import React, { Component } from 'react'
import axios from 'axios';
import 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class ViewerEdit extends Component {
    // creating a normal state where name array initialised
    state = {
        name: '',
        fare: '',
        place: '',
        email: '',
        errors:[]
    }
    handleSubmit = event => {
        event.preventDefault();
        console.log(this.state);
        const user = {
            name: this.state.name,
            fare: this.state.name,
            place: this.state.place,
            email: this.state.email,
        }
        console.log("User Data : "+ JSON.stringify(user));
        axios.post('http://127.0.0.1:8000/api/viewers/add',  this.state).then(response => {
            console.log('Response by the formdata - ' + JSON.stringify(response.data));
            console.log('Response Data by the formdata - ' + response.data);
        }).catch(error=>{
            console.log(error.response.data.errors);
            this.setState({errors:error.response.data.errors})
            console.log("Errors"+JSON.stringify(this.state.errors.name))
        })
        
    }
    handleName=event=>{
        this.setState({name:event.target.value})
    } 
    handleEmail=event=>{
        this.setState({email:event.target.value})
    }
    handleFare=event=>{
        this.setState({fare:event.target.value})
    }
    handlePlace=event=>{
        this.setState({place:event.target.value})
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div><label>Name</label>
                    <input type="text" name="name" onChange={this.handleName} /></div>
                    <div><label>Email</label>
                    <input type="text" name="email" onChange={this.handleEmail} /></div>
                    <div><label>Fare</label>
                    <input type="text" name="fare" onChange={this.handleFare} /></div>
                    <div><label>Price</label>
                    <input type="text" name="place" onChange={this.handlePlace} /></div>
                    <div><button type="submit" name="submit" value="Save Viewer" >Save Viewer</button></div>
                </form>
            </div>
        )
    }
}