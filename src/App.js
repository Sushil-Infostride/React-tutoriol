import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css';
// // import Mounting from './Mounting';
import Viewers from './Viewers';
import ViewerAdd from './ViewerAdd';
import ViewerEdit from './ViewerEdit';
import ProfileComponent from './ProfileComponent';
import EditProfileComponent from './EditProfileComponent';
// import Testcomponent from './Testcomponent';


function App() {
  return (
    <Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <ul className="navbar-nav mr-auto">
            <li><Link to={'/'} className="nav-link"> Home </Link></li>
            <li><Link to={'/add'} className="nav-link">Add More</Link></li>
            <li><Link to={'/about'} className="nav-link">About</Link></li>
          </ul>
          </nav>
          <Switch>
              <Route exact path='/' component={Viewers} />
              <Route path='/add' component={ViewerAdd} />
              <Route exact path='/details/:id' component={ProfileComponent} />
              <Route path='/edit' component={ViewerEdit} />
              <Route path='/profile' component={ProfileComponent} />
              <Route exact path='/updatte/:id' component={EditProfileComponent} />
          </Switch>
      {/* <Viewers /> */}
      {/* <Testcomponent /> */}
     
    </div>
    </Router>
  ); 
}

export default App;
