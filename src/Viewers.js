import React, { Component } from 'react'
import axios from 'axios';
import 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import $ from "jquery";

export default class Viewers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            name: [],
            id: 0,
            sno: 0,
            successMessages: "",
            singleName: [],
        }
    }
    componentDidMount() {
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

        axios.get('http://127.0.0.1:8000/api/viewers/').then(res => {
            // axios.get('https://jsonplaceholder.typicode.com/users').then(res => {
            console.log(res.data)
            this.setState({ name: res.data });
        });
        console.log("I am rendered after the render method")
    }
    onDelete(id) {
        axios.get('http://127.0.0.1:8000/api/viewers/' + id + '/delete').then(response => {
            // axios.get('https://jsonplaceholder.typicode.com/users/' + id).then(response => {
            console.log(response.data);
            this.setState({ success: true })
            this.setState({ successMessages: response.data })

            /* 
            Temporarily deleting the data from JsonPlaceholder not actually deleting but filtering
            const name = this.state.name.filter(item => item.id !== id);
            this.setState({ name: name });
            */

        }).catch(error => {
            console.log(error.message);
        })
    }

    render() {
        let counter = 1
        var colors = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#f1c40f", "#e67e22", "#e74c3c", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"];
        var random_color = colors[Math.floor(Math.random() * colors.length)];
        // document.getElementById('title').style.color = random_color;
        return (

            <div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Avatar</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Fair</th>
                            <th scope="col">Place</th>
                            <th scope="col">Edit List </th>
                            <th scope="col">list info</th>
                        </tr>
                    </thead>
                    <tbody>

                        {this.state.name.map(name =>
                            <tr key={counter++}>
                                <td>{counter}</td>
                                <td><span className="avatar" id="title">{name.name[0].toUpperCase()}</span></td>                                   
                                <td>{name.name}</td>
                                <td>{name.email}</td>
                                <td>{name.fare}</td>
                                <td>{name.place}</td>
                                <td>
                                    <Link to={'/updatte/' + window.btoa(name.id)}><button className="btn btn-sm btn-info"  ><i className="fas fa-info-circle"></i> Edit</button></Link>
                                    <button className="btn btn-sm btn-danger" onClick={this.onDelete.bind(this, name.id)
                                    }><i className="fas fa-trash-alt"></i> delete</button>
                                </td>
                                <td><Link to={'/details/' + window.btoa(name.id)} className="nav-link"><button className="btn btn-sm btn-info"  ><i className="fas fa-info-circle"></i> Details</button></Link></td>
                            </tr>

                        )}

                    </tbody>

                </table>
            </div >
        )
    }
}

