import React, { Component } from 'react'
export default class Mounting extends Component {

  constructor(props) {
    super(props);
    this.state = { favoritecolor: "red" };
  }
  static getDerivedStateFromProps(props, state) {
    return {
      favoritecolor: props.favcol,
      fav: state.favoritecolor + "updated"
    };
  }
  render() {
    return (
      <h1>My Favorite Color is {this.state.favoritecolor} and state derieved is {this.state.fav}</h1>
    );
  }
}

