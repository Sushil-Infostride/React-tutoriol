// import ReactDOM from 'react-dom';
import React, { Component } from 'react'
import axios from 'axios';
import 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AlertComponent from './AlertComponent ';

export default class ViewerAdd extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: true
        }
    }
    // creating a normal state where name array initialised

    state = {
        name: '',       // name
        fare: '',       // fare 
        place: '',      // place
        email: '',      // email
        emailfield: [], // Email error
        farefield: [],  // Fare error
        placefield: [], // Place error
        namefield: []   // Name error
    }
    // Handling the Submit function
    handleSubmit = event => {
        event.preventDefault();
        console.log(this.state);
        const user = {
            name: this.state.name,
            fare: this.state.fare,
            place: this.state.place,
            email: this.state.email,
        }
        console.log("User Data : " + JSON.stringify(user));

        /* 
        // Using Await an Asynchronus callback
         const res = await axios.post('http://127.0.0.1:8000/api/viewers/add', this.state);
         console.log("response " + res.data);
         this.props.history.push("/");
         console.log(res.data.errors) 
         */

        // Using Axios Promises to send post request on API   
        axios.post('http://127.0.0.1:8000/api/viewers/add', this.state).then(response => {
            console.log('Response by the formdata - ' + JSON.stringify(response.data));
            console.log('Response Data by the formdata - ' + response.status + response.data);
            // Candition for data save
            if (response.status === 200) {
                this.setState({ error: false })
                this.props.history.push("/");
            }
            else {
                this.setState({ error: true })
            }
            // Setting the error states for particular fields
        }).catch(error => {
            console.log(error.response.data.errors);
            this.setState({ emailfield: error.response.data.errors.email })
            this.setState({ namefield: error.response.data.errors.name })
            this.setState({ farefield: error.response.data.errors.fare })
            this.setState({ placefield: error.response.data.errors.place })
            // console.log("Errors" + JSON.stringify(this.state.errors));

        })

    }
    // using same function for onchange event
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }
    handleclick = e => {
        this.alert('You cannot change email address')
    }
    render() {
        let { error } = this.state;
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form">
                        <label>Name</label>
                        <div className="form-row">   <input type="text" name="name" className="form-control" onChange={this.handleChange} />
                            {error ? (<AlertComponent name={this.state.namefield} />) : (null)}</div>
                        <div className="form-row"> <label>Email</label>
                            <input type="text" className="form-control" name="email" onChange={this.handleChange} />
                            {error ? (<AlertComponent name={this.state.emailfield} />) : (null)} </div>
                        <div className="form-row"> <label>Fare</label>
                            <input type="text" className="form-control" name="fare" onChange={this.handleChange} />
                            {error ? (<AlertComponent name={this.state.farefield} />) : (null)}</div>
                        <div className="form-row"> <label>Price</label>
                            <input type="text" className="form-control" name="place" onChange={this.handleChange} />
                            {error ? (<AlertComponent name={this.state.placefield} />) : (null)} </div>
                    </div>

                    <div><button type="submit" name="submit" value="Save Viewer" >Save Viewer</button></div>
                </form>

            </div>
        )
    }
}
