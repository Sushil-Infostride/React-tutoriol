import React, { Component } from 'react'
import Mounting from './Mounting'

export default class Testcomponent extends Component {
    constructor(props){
        super(props);
        console.log("I am Constructor :  ");
        this.state={favorite:"Alphabets"}
    }
    render() {
        return (
            <div>
                <Mounting favcol={this.state.favorite}/>
            </div>
        )
    }
}
