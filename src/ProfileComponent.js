import React, { Component } from 'react'
import axios from 'axios';
import { Link } from 'react-router-dom';

export default class ProfileComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: window.atob(this.props.match.params.id),
            data: [],
            // data:'',
        }
    }
    componentDidMount() {
        axios.get('http://127.0.0.1:8000/api/viewers/'+this.state.id+'/show').then(res => {
        // axios.get('https://jsonplaceholder.typicode.com/users/' + this.state.id).then(res => {
            console.log(res.data)
            this.setState({ data: [res.data] });
            // this.setState({ data:res.data });
            console.log(this.state.data)
        });

    }

    render() {
        return (
            <div>
                {this.state.data.map(name =>
                    <div className="row gutters-sm" key="0">
                        <div className="col-md-4 mb-3">
                            <div className="card">
                                <div className="card-body">
                                    <div className="d-flex flex-column align-items-center text-center">
                                        <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" className="rounded-circle" width="150" />
                                        <div className="mt-3">
                                            <h4>{name.name}</h4>
                                            <p className="text-secondary mb-1">{name.website}</p>
                                            <p className="text-muted font-size-sm">{name.place}</p>
                                            <button className="btn btn-primary">Follow</button>
                                            <button className="btn btn-outline-primary">Message</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="card mb-3">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-sm-3">
                                            <h6 className="mb-0">Full Name</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {name.name}
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-sm-3">
                                            <h6 className="mb-0">Email</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {name.email}
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-sm-3">
                                            <h6 className="mb-0">Phone</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {/* {name.phone} */}
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-sm-3 text-center">
                                            <h6 className="mb-0">Company Name</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {/* {name.company.name} */}
                                        </div>
                                        <div className="col-sm-3 text-center">
                                            <h6 className="mb-0">Company Tagline</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {/* {name.company.catchPhrase} */}
                                        </div>
                                        <div className="col-sm-3 text-center">
                                            <h6 className="mb-0">Company Bs</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {/* {name.company.bs} */}
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-sm-3">
                                            <h6 className="mb-0">Address</h6>
                                        </div>
                                        <div className="col-sm-9 text-secondary">
                                            {/* {name.address.street},{name.address.suite},{name.address.city},{name.address.zipcode} */}
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <Link className="btn btn-info "to={'/updatte/'+ window.btoa(name.id)}>Edit</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>

        )
    }
}
