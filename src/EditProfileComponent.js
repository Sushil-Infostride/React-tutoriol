// import ReactDOM from 'react-dom';
import React, { Component } from 'react'
import axios from 'axios';
import 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AlertComponent from './AlertComponent ';

export default class EditProfileComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: true,
            id: window.atob(this.props.match.params.id),
            data:[],
            name: '',       // name
            fare: '',       // fare 
            place: '',      // place
            email: '',      // email
            emailfield: [], // Email error
            farefield: [],  // Fare error
            placefield: [], // Place error
            namefield: []   // Name error
        }
    }

    // creating a normal state where name array initialised

    componentDidMount(){
        console.log(this.state.id)
        axios.get('http://127.0.0.1:8000/api/viewers/'+this.state.id+'/show').then(res => {
        // axios.get('https://jsonplaceholder.typicode.com/users/' + this.state.id).then(res => {
            console.log("Dta"+res.data.name)
            this.setState({ name: res.data.name });
            this.setState({ fare: res.data.fare });
            this.setState({ place: res.data.place });
            this.setState({ email: res.data.email });
            console.log(this.state.name)
            // this.setState({ data:res.data });
            // console.log(this.state.data)
        });
    }
    // Handling the Submit function
    handleSubmit = event => {
        event.preventDefault();
        console.log(this.state);
        const user = {
            name: this.state.name,
            fare: this.state.fare,
            place: this.state.place,
            email: this.state.email,
        }
        console.log("User Data : " + JSON.stringify(user));

        /* 
        // Using Await an Asynchronus callback
         const res = await axios.post('http://127.0.0.1:8000/api/viewers/add', this.state);
         console.log("response " + res.data);
         this.props.history.push("/");
         console.log(res.data.errors) 
         */

        // Using Axios Promises to send post request on API   

        axios.post('http://127.0.0.1:8000/api/viewers/'+this.state.id+'/update', this.state).then(response => {
            console.log('Response by the formdata - ' + JSON.stringify(response.data));
            console.log('Response Data by the formdata - ' + response.status + response.data);
            // Candition for data save
            if (response.status === 200) {
                this.setState({ error: false })
                this.props.history.push("/");
            }
            else {
                this.setState({ error: true })
            }
            // Setting the error states for particular fields
        }).catch(error => {
            console.log(error.response.data.errors);
            this.setState({ emailfield: error.response.data.errors.email })
            this.setState({ namefield: error.response.data.errors.name })
            this.setState({ farefield: error.response.data.errors.fare })
            this.setState({ placefield: error.response.data.errors.place })
            // console.log("Errors" + JSON.stringify(this.state.errors));

        })

    }
    // using same function for onchange event
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }
    handleclick = e => {
       alert('You cannot change email address')
     }
    render() {
        let { error } = this.state;
        return (
            <div>
                {/* {this.state.data.map(name => */}
                <form onSubmit={this.handleSubmit}>
                    <div>
                        <label>Name</label>
                        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                        {error ? (<AlertComponent name={this.state.namefield} />) : (null)}
                    </div>
                    <div><label>Email</label>
                        <input type="text" name="email" value={this.state.email} readOnly onDoubleClick={this.handleclick} />
                        {error ? (<AlertComponent name={this.state.emailfield} />) : (null)}
                    </div>
                    <div><label>Fare</label>
                        <input type="text" name="fare"value={this.state.fare} onChange={this.handleChange} />
                        {error ? (<AlertComponent name={this.state.farefield} />) : (null)}
                    </div>
                    <div><label>Price</label>
                        <input type="text" name="place" value={this.state.place} onChange={this.handleChange} />
                        {error ? (<AlertComponent name={this.state.placefield} />) : (null)}
                    </div>
                    <div><button type="submit" name="submit" value="Save Viewer" >Save Viewer</button></div>
                </form>
                {/* )} */}
            </div>
        )
    }
}
